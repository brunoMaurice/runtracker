// @ts-ignore
import L from "leaflet";
import * as React from "react";
import { GeoJSON, Map, Marker, Popup, TileLayer } from "react-leaflet";
import geoData from "../../geoJson/ecotrail2019";

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png")
});

interface IState {
  lat: number;
  lng: number;
  zoom: number;
  currentPos: {
    lat: number;
    lng: number;
    username: string;
  } | null;
  message: string;
}

const MESSAGES_LIST = [
  "Je suis arrivé jusqu'à ici!",
  "Awesome!",
  "🎼Un kilomètre à pied ça use les souliers! ♪♪♪",
  "(ง •̀_•́)ง",
  "ヽ(。_°)ノ",
  "Hey baby, how you doin' (☞⌐■_■)☞",
  "🎼 Run boy run! Running is a victory 🎶",
  "🎼 Run boy run! This world is not made for you 🎶",
  "🎼 Run boy run! This ride is a journey to 🎶",
  "🚀 Mayday Mayday 🚀",
  "Il court, il court, le furet ♪♪♪",
  "Mais qu'elle idée 😓",
  "🏁C'est que le début... 🙀",
  "Oh la belle ampoule 😮",
  "On est bientôt arrivé ?",
  "Et là ? Toujours pas arrivé ?",
  "Là c'est la pause 💩",
  "La bière c'est bientôt",
  "Les courbatures c'est demain et maintenant",
  "On peu toujours faire plus 🤪",
  "Courir c'est ma passion mais la j'en ai marre 🙃",
  "Jean et Marre sont dans un bateau, Jean tombe à l'eau, Marre est toujours là",
  "Toujours plus 💨",
  "Toujours plus haut, toujours plus loin 💨",
  "Même pas mal 🤯",
  "Demain on refait ?",
  "Bonjour, la tour et effel c'est par ici ?",
  "Je vais peux être prendre le bus",
  "L'année prochain la Diagonale des fous ? 🤩",
  "Je veux rentrer à la maison 🥺",
  "J'ai beau être matinal j'ai mal 😨",
  "🤖La douleur est qu'un signal envoyé a mon cerveau que je veux pas comprendre 🤖",
  "On fait la fête ? 🥳",
  "J'ai froid alors je cours 🥶",
  "Dormir 😴",
  "Plus jamais ça 🤥",
  "Je fais quoi ici ? 🤔",
  "Même pas mal 🤯",
  "Je vais retourner au jeu vidéo 👾",
  "Je suis mort ☠",
  "Je vous aime 🥰",
  "Marche ou crêve",
  "🗣 A l'aide"
];

class MapContent extends React.Component<{}, IState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      lat: 48.8090676,
      lng: 2.1734399,
      zoom: 12,
      currentPos: null,
      message: ""
    };
  }

  public async loadData() {
    const responses = await fetch(`/api/lastPos`, {
      method: "GET"
    });

    this.setState({
      ...this.state,
      currentPos: await responses.json(),
      message: MESSAGES_LIST[Math.floor(Math.random() * MESSAGES_LIST.length)]
    });
  }

  public async componentDidMount() {
    await this.loadData();
    setInterval(async () => {
      await this.loadData();
    }, 120000); // 2 minutes in milliseconds
  }

  public render() {
    const position: [number, number] = [this.state.lat, this.state.lng];
    return (
      <Map center={position} zoom={this.state.zoom}>
        <GeoJSON data={geoData} />
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {this.state.currentPos !== null && (
          <Marker position={this.state.currentPos}>
            <Popup>
              {this.state.message} - {this.state.currentPos.username}
            </Popup>
          </Marker>
        )}
      </Map>
    );
  }
}

export default MapContent;
