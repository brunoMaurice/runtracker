import * as React from "react";
import Button, { BUTTON_TAGS } from "../button/Button";

interface IStats {
  email: string;
}

class AuthForm extends React.Component<{}, IStats> {
  constructor(props: {}) {
    super(props);

    this.state = {
      email: ""
    };
  }

  public async sendConnectLink(email: string): Promise<void> {
    const responses = await fetch(`/api/connect`, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify({ email })
    });

    console.error(responses);
  }

  public render() {
    return (
      <form className="db">
        <div className="mv3 dib">
          <input
            type="text"
            name="email"
            placeholder="Your account email"
            className="db w-100 pa2 mt2 br2 b--black-20 ba f6"
            value={this.state.email}
            onChange={e => {
              this.setState({
                ...this.state,
                email: e.target.value
              });
            }}
          />
        </div>

        <Button
          tags={BUTTON_TAGS.PRIMARY}
          onClick={async () => {
            await this.sendConnectLink(this.state.email);
          }}
        >
          Send connect link
        </Button>
      </form>
    );
  }
}

export default AuthForm;
