import * as React from "react";

class Footer extends React.Component {
  public render() {
    return (
      <footer className="tc pa3 ph5-ns bt b--black-10">
        <span>Run Tracker © {new Date().getUTCFullYear()}</span>
      </footer>
    );
  }
}

export default Footer;
