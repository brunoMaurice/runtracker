module.exports = function(api) {
  const presets = [
    ["@babel/preset-env", { useBuiltIns: "entry", corejs: "3.0.0" }],
    "@babel/preset-react",
    "@babel/preset-typescript"
  ];

  const plugins = ["@babel/plugin-proposal-class-properties"];

  api.cache.never();

  return { presets, plugins };
};
