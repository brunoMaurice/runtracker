#!/bin/sh

if docker ps -a --format '{{.Names}}' | grep -Eq "^postgres\$"; then
  docker stop postgres
fi