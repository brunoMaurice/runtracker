# Run Tracker

This application allow you to receive gps position from any application and show it on a map.
You will be able to share your position during your running session.

## Installation

Use the package manager [yarn](https://yarnpkg.com/fr/) to install js package
And [make]() to handle project command

Copie the `.env.sample` to `.env` and edit the env propertie

```bash
make install.dev
```

## Usage

To start the application :

```nodejs
make start
```

## Contributing

Pull requests are welcome. 
For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
