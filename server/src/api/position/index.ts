import { Application, Request, Response } from "express";
import { Position } from "../../db/models/position";
import logger from "./../../services/logger";

export const apiPosition = (srv: Application) => {
  srv.get(
    "/api/log",
    async (req, res): Promise<void> => {
      try {
        const positionRepo = req.container.getRepository(Position);
        const position = await positionRepo.save(
          positionRepo.create({
            lat: req.query.lat,
            lng: req.query.lng,
            user: req.query.user
          })
        );

        res.send(position);
      } catch (err) {
        logger(err);
        res.sendStatus(500);
      }
    }
  );

  srv.get(
    "/api/lastPos",
    async (req: Request, res: Response): Promise<void> => {
      try {
        const positions = await req.container.getRepository(Position).find({
          order: {
            createdDate: "DESC"
          },
          take: 1
        });

        if (positions.length === 1) {
          res.send(positions[0]);
        } else {
          res.sendStatus(404);
        }
      } catch (err) {
        logger(err);
        res.sendStatus(500);
      }
    }
  );
};
