import {
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

export abstract class AbstractEntity {
  @PrimaryGeneratedColumn("uuid")
  public uuid: string;
  @CreateDateColumn({ readonly: true })
  public createdDate: Date;
  @UpdateDateColumn({
    default: () => "now()",
    nullable: false
  })
  public updatedDate: Date;
}
