require("dotenv").config();

console.log(`Database progres ${process.env.DATABASE_CONNECTION_URI}`);

module.exports = [
  Object.assign(
    {},
    {
      cli: {
        migrationsDir: "src/db/migrations"
      },
      entities: ["src/db/models/**/*.ts"],
      logging: true,
      migrations: ["src/db/migrations/**/*.ts"],
      name: "default",
      type: "postgres"
    },
    {
      url: process.env.DATABASE_CONNECTION_URI
    }
  )
];
