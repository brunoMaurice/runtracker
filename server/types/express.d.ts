import { IAuth } from "./../src/api/middleware/auth";
import { Container } from "./../src/services/injection/container";

declare global {
  namespace Express {
    export interface Request {
      container: Container;
      auth: IAuth;
    }
  }
}
