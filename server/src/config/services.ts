import { Connect } from "../services/auth/connect";
import { MailClient } from "../services/mail/client";

export const servicesList: { [key: string]: any } = {
  Connect,
  MailClient
};
