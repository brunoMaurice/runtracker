import { Application, NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { promisify } from "util";
import { AUTH_SECRET } from "../../config";
import { User } from "../../db/models/user";

const jwtVerify = promisify<{}, {}, IJwtUser>(jwt.verify);

const bearerLength = "Bearer ".length;

const getTokenFromAuthHeader = (header?: string) => {
  if (header && header.length > bearerLength) {
    return header.slice(bearerLength);
  }
  return null;
};

export const getUserFromAuthHeader = async (
  secretOrPublicKey: string,
  authorization?: string
): Promise<IJwtUser | null> => {
  const token = getTokenFromAuthHeader(authorization);
  if (token) {
    try {
      return jwtVerify(token, secretOrPublicKey);
    } catch (e) {
      return null;
    }
  }

  return null;
};

export interface IAuth0Identity {
  connection: string;
  isSocial: boolean;
  provider: string;
  user_id: string;
}

export interface IJwtUser {
  name: string;
  nickname: string;
  picture: string;
  user_id: string;
  username?: string;
  given_name?: string;
  family_name?: string;
  email?: string;
  email_verified?: boolean;
  clientID: string;
  gender?: string;
  locale?: string;
  identities: IAuth0Identity[];
  created_at: string;
  updated_at: string;
  sub: string;
  user_metadata?: any;
  app_metadata?: any;
}

export interface IAuth {
  jwt: IJwtUser | null;
  user: User;
}

export const AuthMiddleware = (srv: Application) => {
  srv.use("/api/*", async (req: Request, res: Response, next: NextFunction) => {
    try {
      const authorization = req && req.headers && req.headers.authorization;
      const jwtUser: IJwtUser | null = await getUserFromAuthHeader(
        AUTH_SECRET,
        authorization
      );
      let user: User;

      if (jwtUser) {
        user = await req.container
          .getRepository(User)
          .findOneOrFail(jwtUser.clientID);
      } else {
        user = new User();
      }

      req.auth = {
        jwt: jwtUser,
        user
      };

      return next();
    } catch (err) {
      return res.sendStatus(500);
    }
  });
};
