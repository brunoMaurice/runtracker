######################################
##     MAKEFILE FOR RUN TRACKER     ##
######################################

include .env

COLOR=\033[;32m
NC=\033[0m

.PHONY: install.server install.front install start db.migrate

all: help

help:
	@echo "------------------------------------"
	@echo "Run ${COLOR}make install${NC} to install project dependencies"
	@echo "Run ${COLOR}make start${NC} to build and start container"
	@echo "Run ${COLOR}make dev${NC} to build and run container at every change"
	@echo "Run ${COLOR}make lint${NC} to run prettier and linter check"
	@echo "Run ${COLOR}make lint.fix${NC} to run prettier and linter fix"
	@echo "Run ${COLOR}make db.install${NC} to install a docker db"
	@echo "Run ${COLOR}make db.migrate${NC} to run db migrations"
	@echo "------------------------------------"

# Install

install.server:
	cd server && yarn install --pure-lockfile

install.front:
	cd front && yarn install --pure-lockfile

install:
	$(MAKE) install.server
	$(MAKE) install.front
	cp .env front/.env
	cp .env server/.env

# Service management

start:
	cd front && yarn build
	cd server && yarn build
	cd server && yarn start

dev:
	cd front && yarn watch &
	cd server && yarn watch

lint:
	cd front && yarn lint &
	cd server && yarn lint

lint.fix:
	cd front && yarn lint:fix &
	cd server && yarn lint:fix

# Database

db.install:
	cd server && yarn db:reset

db.migrate:
	cd server && yarn db:migrate