import { Column, Entity, ManyToOne } from "typeorm";
import { AbstractEntity } from "./abstractEntity";
import { User } from "./user";

@Entity()
export class Position extends AbstractEntity {
  @Column({ nullable: false })
  public lat: string;

  @Column({ nullable: false })
  public lng: string;

  @ManyToOne(() => User, user => user.uuid, { nullable: false })
  public user: User;
}
