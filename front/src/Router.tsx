import * as React from "react";
import { Route } from "react-router-dom";

export interface IROUTE {
  path: string;
  component: string;
  exact?: boolean;
}

export enum ROUTES {
  HOME = "/",
  MAP = "/map",
  LOGIN = "/login",
  AUTHTOKEN = "/loginToken"
}

export const routesConfig: IROUTE[] = [
  {
    component: ROUTES.HOME,
    path: ROUTES.HOME,
    exact: true
  },
  {
    component: ROUTES.MAP,
    path: ROUTES.MAP,
    exact: true
  },
  {
    component: ROUTES.LOGIN,
    path: ROUTES.LOGIN,
    exact: true
  },
  {
    component: ROUTES.AUTHTOKEN,
    path: "/login/:token"
  }
];

class Routes extends React.Component {
  public render() {
    return (
      <>
        {routesConfig.map((route, i) => {
          const path =
            route.component.charAt(0) === "/"
              ? route.component.substr(1)
              : route.component;
          const component = require(`./pages/${path}`).default;
          return (
            <Route
              exact={route.exact}
              path={route.path}
              component={component}
              key={i}
            />
          );
        })}
      </>
    );
  }
}

export default Routes;
