#!/bin/sh

set -e # exit on error

docker run --name postgres -e POSTGRES_PASSWORD=runtracker -e POSTGRES_DB=runtracker -e POSTGRES_USER=runtracker -d --rm -p 5432:5432 postgres

echo "Waiting for postgres to start on host localhost..."
export RETRIES=80
until docker run --net=host --name postgres-startup-watcher --rm postgres sh -c "PGPASSWORD=runtracker psql -h localhost -p 5432 -U runtracker runtracker -c 'select 1'" > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
  echo "Waiting for postgres server to start, $((RETRIES)) remaining attempts..."
  RETRIES=$((RETRIES-=1))
  sleep 1
done
echo "...OK"