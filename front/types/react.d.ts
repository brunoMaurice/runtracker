import { ComponentClass, ReactNode, StatelessComponent } from "react";

declare module "react" {
  export type AnyComponent<TProps> =
    | ComponentClass<TProps>
    | StatelessComponent<TProps>;
  export type AnyChildren = ReactNode | ReactNode[];
}

// vs code issue fixe :
export type AnyComponent<TProps> =
  | ComponentClass<TProps>
  | StatelessComponent<TProps>;
export type AnyChildren = ReactNode | ReactNode[];
