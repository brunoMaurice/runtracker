import * as React from "react";
import { Link } from "react-router-dom";
import Button from "./Button";

class LinkButton extends React.Component<{
  to: string;
  children: React.ReactNode;
}> {
  public render() {
    return (
      <Link to={this.props.to}>
        <Button>{this.props.children}</Button>
      </Link>
    );
  }
}

export default LinkButton;
