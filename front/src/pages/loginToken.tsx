import * as React from "react";
import { RouteComponentProps } from "react-router";
import { setAuthToken } from "../components/auth/auth";
import Page from "../components/layout/Page";

interface IRouteProps {
  token: string;
}

class Login extends React.Component<RouteComponentProps<IRouteProps>, {}> {
  constructor(props: any) {
    super(props);

    this.state = {};
  }

  public async componentDidMount() {
    const responses = await fetch(`/api/auth`, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify({ token: this.props.match.params.token })
    });

    setAuthToken(await responses.text());
  }

  public render() {
    return (
      <Page isPublic>
        <div className="tc">Happy to see you back</div>
      </Page>
    );
  }
}

export default Login;
