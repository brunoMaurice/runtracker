import "react";

// Augmentation of React
declare module "react" {
  export interface StyleHTMLAttributes<T> extends React.HTMLAttributes<T> {
    jsx?: boolean;
    global?: boolean;
  }
}

export interface StyleHTMLAttributes<T> extends React.HTMLAttributes<T> {
  jsx?: boolean;
  global?: boolean;
}
