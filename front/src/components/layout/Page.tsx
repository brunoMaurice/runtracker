import * as React from "react";
import { Redirect } from "react-router";
import { ROUTES } from "../../Router";
import { hasJwt } from "../auth/auth";
import Footer from "./Footer";
import Header from "./Header";

interface IProps {
  children: React.AnyChildren;
  isPublic?: boolean;
}

interface IState {
  redirectToReferrer: boolean;
}

class Page extends React.Component<IProps, IState> {
  public state = { redirectToReferrer: false };

  public async componentDidMount() {
    if (!this.props.isPublic && !hasJwt()) {
      this.setState({ redirectToReferrer: true });
    }
  }

  public render() {
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={ROUTES.LOGIN} />;
    }

    return (
      <div className="App helvetica bg-near-white black-70 vh-100 flex flex-column">
        <Header />
        <main className="flex-1">{this.props.children}</main>
        <Footer />
      </div>
    );
  }
}

export default Page;
