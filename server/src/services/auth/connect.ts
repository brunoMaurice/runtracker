import { inject, injectable } from "inversify";
import * as jwt from "jsonwebtoken";
import * as moment from "moment";
import { Repository } from "typeorm";
import * as uuid from "uuid/v4";
import { AUTH_SECRET, REACT_APP_PROJECT_URI } from "../../config";
import { Auth } from "../../db/models/auth";
import { User } from "../../db/models/user";
import logger from "../logger";
import { MailClient } from "../mail/client";
import { authTpl } from "../mail/tpl/auth";

@injectable()
export class Connect {
  @inject("AuthRepository")
  public authRepo: Repository<Auth>;
  @inject("MailClient")
  public mailClient: MailClient;

  // Generate a token and send connect link by email
  public async sendTokenLink(user: User): Promise<boolean> {
    const auth = await this.authRepo.findOne({ where: { user } });

    // link available 2h
    const expirationDate = new Date(new Date().getTime() + 60 * 60000 * 2);
    const token = uuid();

    if (auth) {
      this.authRepo.update(auth.uuid, { token, expirationDate });
    } else {
      await this.authRepo.insert(
        this.authRepo.create({
          user: { uuid: user.uuid },
          token,
          expirationDate
        })
      );
    }

    logger(`Login token: ${token}`);
    this.mailClient.send(
      user,
      "Welcome Back",
      { loginUri: `${REACT_APP_PROJECT_URI}/login/${token}` },
      authTpl
    );

    return true;
  }

  // Valid a token and return the jwt session
  public async connectUser(token: string): Promise<string | null> {
    const auth = await this.authRepo.findOne({
      where: { token }
    });

    if (!auth) {
      return null;
    }

    if (moment(auth.expirationDate).isBefore(moment())) {
      return null;
    }

    const jwtToken = jwt.sign(
      {
        iat: moment().valueOf(),
        exp: moment()
          .add(1, "days")
          .valueOf()
      },
      AUTH_SECRET
    );

    return jwtToken;
  }
}
