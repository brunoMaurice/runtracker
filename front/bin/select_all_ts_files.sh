#!/bin/sh

find ./src -type f -name '*.ts' | grep -v typings
find ./src -type f -name '*.tsx' | grep -v typings
