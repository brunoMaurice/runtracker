import * as React from "react";
import AuthForm from "../components/auth/AuthForm";
import Page from "../components/layout/Page";

class Login extends React.Component {
  public render() {
    return (
      <Page isPublic>
        <div className="tc">
          <h1>Happy to see your again !</h1>
          <p>Enter your account email. We will send you the connect link !</p>
          <AuthForm />
        </div>
      </Page>
    );
  }
}

export default Login;
