import * as React from "react";

// @ts-ignore
const isServer = (): boolean => (!process.browser ? true : false);

export const GLOBAL_CONFIG_KEY = "APP_CONFIG";

function getEnv(name: string, defaultValue?: any): string {
  let config = null;
  if (isServer()) {
    config = process.env[name];
    // @ts-ignore
  } else if (window[GLOBAL_CONFIG_KEY]) {
    // @ts-ignore
    config = window[GLOBAL_CONFIG_KEY][name];
  }
  if (
    (!config && config !== false && config !== "false") ||
    (config + "").length <= 0
  ) {
    if (defaultValue !== undefined) {
      return defaultValue;
    }
    // throw new Error(`Undefined config ${name}`);
  }
  return config + "";
}

export const REACT_APP_PROJECT_URI: string = getEnv(
  "REACT_APP_PROJECT_URI",
  process.env.REACT_APP_PROJECT_URI
);
export const REACT_APP_API_URI: string = getEnv(
  "REACT_APP_API_URI",
  process.env.REACT_APP_API_URI
);

const allConfigs = { REACT_APP_PROJECT_URI, REACT_APP_API_URI };

export function Config() {
  return (
    <script
      type="text/javascript"
      dangerouslySetInnerHTML={{
        __html:
          `window[${JSON.stringify(GLOBAL_CONFIG_KEY)}] = {};` +
          Object.keys(allConfigs)
            .map(
              key =>
                `window[${JSON.stringify(GLOBAL_CONFIG_KEY)}][${JSON.stringify(
                  key
                )}] = ${
                  // @ts-ignore
                  JSON.stringify(allConfigs[key])
                };`
            )
            .join("")
      }}
    />
  );
}
