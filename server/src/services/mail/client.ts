import { injectable } from "inversify";
import { TemplateOptions } from "squirrelly";
import { MAILJET_ENABLE } from "../../config";
import { User } from "../../db/models/user";
import { Mailjet } from "./mailjet";

export interface IMailTemplate {
  text: string;
  html: string;
}

@injectable()
export class MailClient {
  public async send(
    user: User,
    subject: string,
    args: TemplateOptions,
    tpl: IMailTemplate
  ): Promise<boolean> {
    if (MAILJET_ENABLE) {
      const mailjetService = new Mailjet();
      return mailjetService.send(user, subject, args, tpl);
    }

    throw new Error("need to setup a mail provider and enable it");
  }
}
