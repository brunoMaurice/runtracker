import { Container as InversifyContainer } from "inversify";
import { servicesList } from "../../config/services";
import { InstanciableService } from "./container";

export class MockableServiceContainer {
  private container: InversifyContainer;

  constructor() {
    this.container = new InversifyContainer();
    this.initServices();
  }

  /**
   * Get a service define in the config/services.ts
   */
  public getService = <T>(service: InstanciableService<T>): T => {
    return this.container.get<T>(service.name);
  };

  /**
   * Allow mocking of any service
   * Only used in unit tests
   */
  public setServiceMock = <T>(
    serviceClass: InstanciableService<T>,
    mockedServiceClassName?: string
  ) => {
    const name = mockedServiceClassName || serviceClass.name;
    return this.container.rebind<T>(name).to(serviceClass);
  };

  public setInstanciateServiceMock = <T>(
    serviceClass: T,
    mockedServiceClassName: string
  ) => {
    const name = mockedServiceClassName;
    return this.container.rebind<T>(name).toConstantValue(serviceClass);
  };

  private initServices = (): void => {
    for (const service in servicesList) {
      if (!servicesList.hasOwnProperty(service)) {
        continue;
      }

      // No context available, so factory can't be init
      // to do when it will be necessary to mock a service instanciate with factory
      if (servicesList[service].kind === "factory") {
        continue;
      } else if (servicesList[service].kind === "instanceFactory") {
        continue;
      } else {
        this.bindClass(servicesList[service]);
      }
    }
  };

  private bindClass = <T>(service: new () => T): void => {
    this.container.bind<T>(service.name).to(service);
  };
}
