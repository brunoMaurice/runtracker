import { IMailTemplate } from "../client";

export const authTpl: IMailTemplate = {
  text: "Welcome Back, Here is your login uri : {{loginUri}}",
  html: `<h1>Welcome Back</h1>,
    <br/> Here is your login uri : <a href="{{loginUri}}">Connect</a>
    <br/> If the link don't work copy colle the link : {{loginUri}}`
};
