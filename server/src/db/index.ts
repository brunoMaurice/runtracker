import "reflect-metadata";
import { Connection, createConnection, Repository } from "typeorm";
import { DATABASE_CONNECTION_URI } from "../config";
import { AbstractEntity } from "./models/abstractEntity";
import { Auth } from "./models/auth";
import { Position } from "./models/position";
import { Route } from "./models/route";
import { User } from "./models/user";

export interface IModels {
  Auth: Repository<Auth>;
  Route: Repository<Route>;
  Position: Repository<Position>;
  User: Repository<User>;
  [key: string]: Repository<any & AbstractEntity>;
}

let connection: Connection;

export const initConnection = async (): Promise<Connection> => {
  if (!connection || !connection.isConnected) {
    connection = await createConnection({
      entities: [Auth, Route, Position, User],
      logging: true,
      synchronize: false,
      type: "postgres",
      url: DATABASE_CONNECTION_URI
    });
  }
  return connection;
};

export const getRepositories = async (): Promise<IModels> => {
  if (!connection || connection === null || connection === undefined) {
    await initConnection();
  }

  return {
    Auth: connection.getRepository(Auth),
    Route: connection.getRepository(Route),
    Position: connection.getRepository(Position),
    User: connection.getRepository(User)
  };
};

export default getRepositories;
