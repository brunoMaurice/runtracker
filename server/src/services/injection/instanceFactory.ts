import { IServiceFactory } from "./serviceFactory";

export abstract class InstanceFactory implements IServiceFactory {
  public kind = "instanceFactory";
  public abstract setDependency = (): void => {};
}
