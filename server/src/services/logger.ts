import debug from "debug";

const runtrackerServerLog = debug("runtracker-server");

export default runtrackerServerLog;
