import * as React from "react";
import { ROUTES } from "../../Router";
import LinkButton from "../button/LinkButton";

class Header extends React.Component {
  public render() {
    return (
      <header className="w-100 pa3 ph5-ns bb b--black-10">
        <div className="db dt-ns mw9 center w-100">
          <div className="db dtc-ns v-mid tl w-50">
            <a
              href={ROUTES.HOME}
              title="home"
              className="dib f5 f4-ns fw6 mt0 mb1 link black-70"
            >
              Run Tracker
            </a>
          </div>
          <nav className="db dtc-ns v-mid w-100 tl tr-ns mt2 mt0-ns">
            <LinkButton to={ROUTES.LOGIN}>Log In</LinkButton>
          </nav>
        </div>
      </header>
    );
  }
}

export default Header;
