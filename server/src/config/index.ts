function testConfig(config: any, name: string) {
  if (
    (!config && config !== false && config !== "false") ||
    (config + "").length <= 0
  ) {
    throw new Error(
      `Undefined config ${name}: ${JSON.stringify(process.env)} `
    );
  }
}

export const DATABASE_CONNECTION_URI: string =
  process.env.DATABASE_CONNECTION_URI || "";

testConfig(DATABASE_CONNECTION_URI, "DATABASE_CONNECTION_URI");

export const AUTH_SECRET: string = process.env.AUTH_SECRET || "";

testConfig(AUTH_SECRET, "AUTH_SECRET");

export const CREATE_USER_ON_LOGIN: boolean =
  process.env.CREATE_USER_ON_LOGIN === "true" || false;

testConfig(CREATE_USER_ON_LOGIN, "CREATE_USER_ON_LOGIN");

export const MAILJET_ENABLE: boolean =
  process.env.MAILJET_ENABLE === "true" || false;

testConfig(MAILJET_ENABLE, "MAILJET_ENABLE");

export const MAILJET_APIKEY: string = process.env.MAILJET_APIKEY || "";

testConfig(MAILJET_APIKEY, "MAILJET_APIKEY");

export const MAILJET_APISECRET: string = process.env.MAILJET_APISECRET || "";

testConfig(MAILJET_APISECRET, "MAILJET_APISECRET");

export const MAILJET_SENDER_EMAIL: string =
  process.env.MAILJET_SENDER_EMAIL || "";

testConfig(MAILJET_SENDER_EMAIL, "MAILJET_SENDER_EMAIL");

export const MAILJET_SENDER_NAME: string =
  process.env.MAILJET_SENDER_NAME || "";

testConfig(MAILJET_SENDER_NAME, "MAILJET_SENDER_NAME");

export const REACT_APP_PROJECT_URI: string =
  process.env.REACT_APP_PROJECT_URI || "";

testConfig(REACT_APP_PROJECT_URI, "REACT_APP_PROJECT_URI");
