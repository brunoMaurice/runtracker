import { Application, NextFunction, Request, Response } from "express";
import { Container } from "../../services/injection/container";
import logger from "../../services/logger";

export const ContainerMiddleware = (srv: Application) => {
  srv.use("/api/*", async (req: Request, res: Response, next: NextFunction) => {
    try {
      const container: Container = new Container();
      await container.init();

      req.container = container;

      return next();
    } catch (err) {
      logger(err);
      return res.sendStatus(500);
    }
  });
};
