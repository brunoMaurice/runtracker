import { Column, Entity, JoinColumn, OneToMany, OneToOne } from "typeorm";
import { AbstractEntity } from "./abstractEntity";
import { Route } from "./route";

@Entity()
export class User extends AbstractEntity {
  @Column({ nullable: false })
  public email: string;

  @Column({ nullable: false })
  public name: string;

  @Column({ nullable: false })
  public trackingToken: string;

  @OneToMany(type => Route, route => route.createdBy)
  public routes: Route[];

  @OneToOne(type => Route, route => route.uuid)
  @JoinColumn()
  public displayRoute: Route;
}
