import * as Cookie from "js-cookie";
import * as jwtDecode from "jwt-decode";
import * as moment from "moment";
import { BehaviorSubject } from "rxjs";

interface IJWTObject {
  iat: number;
  exp: number;
}

const COOKIENAME = "jwt-runtracker";
export const connectedState = new BehaviorSubject(false);

export const logout = () => {
  Cookie.remove(COOKIENAME);
};

const isJwtValid = (jwt: string | undefined): boolean => {
  if (!jwt) {
    return false;
  }

  let decoded;

  try {
    decoded = jwtDecode<IJWTObject>(jwt);
  } catch (e) {
    return false;
  }

  if (!decoded || !decoded.exp) {
    return false;
  }

  const exp = new Date(decoded.exp * 1000);
  const now = new Date();

  return now < exp;
};

export const setAuthToken = (token: string) => {
  if (!isJwtValid(token)) {
    connectedState.next(false);
    return false;
  }

  Cookie.set(COOKIENAME, token);
  connectedState.next(true);
  return true;
};

export const getJwt: () => string | null = () => {
  const jwt = Cookie.get(COOKIENAME);
  const valid = jwt && isJwtValid(jwt) ? jwt : null;
  connectedState.next(!!valid);
  return valid;
};

export const hasJwt: () => boolean = () => !!getJwt();
