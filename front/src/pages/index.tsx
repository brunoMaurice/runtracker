import * as React from "react";
import Page from "../components/layout/Page";
import HomeContent from "./../components/home/HomeContent";

class Home extends React.Component {
  public render() {
    return (
      <Page isPublic>
        <HomeContent />
      </Page>
    );
  }
}

export default Home;
