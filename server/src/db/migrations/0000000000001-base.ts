import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration0000000000001 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "user" (
          "uuid" "uuid" DEFAULT "public"."uuid_generate_v4"() PRIMARY KEY,
          "createdDate" timestamp without time zone NOT NULL DEFAULT now(), 
          "updatedDate" timestamp without time zone NOT NULL DEFAULT now(),
          "email" varchar NOT NULL,
          "name" varchar NOT NULL,
          "trackingToken" varchar NOT NULL,
          "displayRouteUuid" "uuid"
      )`
    );

    await queryRunner.query(
      `CREATE TABLE "auth" (
        "uuid" "uuid" DEFAULT "public"."uuid_generate_v4"() PRIMARY KEY,
          "createdDate" timestamp without time zone NOT NULL DEFAULT now(), 
          "updatedDate" timestamp without time zone NOT NULL DEFAULT now(),
          "userUuid" "uuid" NOT NULL,
          "token" varchar NOT NULL,
          "expirationDate" timestamp without time zone NOT NULL
      )`
    );

    await queryRunner.query(
      `CREATE TABLE "route" (
        "uuid" "uuid" DEFAULT "public"."uuid_generate_v4"() PRIMARY KEY,
          "createdDate" timestamp without time zone NOT NULL DEFAULT now(), 
          "updatedDate" timestamp without time zone NOT NULL DEFAULT now(),
          "createdByUuid" "uuid" NOT NULL,
          "name" varchar NOT NULL,
          "geoJson" varchar NOT NULL
      )`
    );

    await queryRunner.query(
      `CREATE TABLE "position" (
        "uuid" "uuid" DEFAULT "public"."uuid_generate_v4"() PRIMARY KEY,
          "createdDate" timestamp without time zone NOT NULL DEFAULT now(), 
          "updatedDate" timestamp without time zone NOT NULL DEFAULT now(),
          "userUuid" "uuid" NOT NULL,
          "lat" varchar NOT NULL,
          "lng" varchar NOT NULL
      )`
    );

    await queryRunner.query(`
      ALTER TABLE "user" ADD CONSTRAINT "FK_fb13a0f5645bc4e44fbc7016f54" FOREIGN KEY ("displayRouteUuid") 
        REFERENCES "route" ("uuid")
    `);

    await queryRunner.query(`
      ALTER TABLE "auth" ADD CONSTRAINT "FK_3ff4ca6607345724557de0f5ce9" FOREIGN KEY ("userUuid") 
        REFERENCES "user" ("uuid")
    `);

    await queryRunner.query(`
      ALTER TABLE "position" ADD CONSTRAINT "FK_ddcbb670040d3ee6f0dd58afeb5" FOREIGN KEY ("userUuid") 
        REFERENCES "user" ("uuid")
    `);

    await queryRunner.query(`
      ALTER TABLE "route" ADD CONSTRAINT "FK_e8965666d8be085006562d5e5a1" FOREIGN KEY ("createdByUuid") 
        REFERENCES "user" ("uuid")
    `);

    await queryRunner.query(`
      ALTER TABLE "user" ADD CONSTRAINT "REL_fb13a0f5645bc4e44fbc7016f5" UNIQUE ("displayRouteUuid")
    `);

    await queryRunner.query(`
      ALTER TABLE "auth" ADD CONSTRAINT "REL_3ff4ca6607345724557de0f5ce" UNIQUE ("userUuid")
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "position"`);
    await queryRunner.query(`DROP TABLE "auth"`);
    await queryRunner.query(`DROP TABLE "user"`);
    await queryRunner.query(`DROP TABLE "route"`);
  }
}
