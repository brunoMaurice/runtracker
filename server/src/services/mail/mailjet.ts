import * as mailjet from "node-mailjet";
import * as Sqrl from "squirrelly";
import {
  MAILJET_APIKEY,
  MAILJET_APISECRET,
  MAILJET_SENDER_EMAIL,
  MAILJET_SENDER_NAME
} from "../../config";
import { User } from "../../db/models/user";
import logger from "../logger";
import { IMailTemplate } from "./client";

export class Mailjet {
  public async send(
    user: User,
    subject: string,
    args: Sqrl.TemplateOptions,
    tpl: IMailTemplate
  ): Promise<boolean> {
    const mailjetClient = mailjet.connect(MAILJET_APIKEY, MAILJET_APISECRET);

    try {
      await mailjetClient.post("send", { version: "v3.1" }).request({
        Messages: [
          {
            From: {
              Email: MAILJET_SENDER_EMAIL,
              Name: MAILJET_SENDER_NAME
            },
            To: [
              {
                Email: user.email,
                Name: user.name
              }
            ],
            Subject: subject,
            TextPart: Sqrl.Render(tpl.text, args),
            HTMLPart: Sqrl.Render(tpl.html, args)
          }
        ]
      });

      return true;
    } catch (e) {
      logger(`mailjet error :`, e);
      return false;
    }
  }
}
