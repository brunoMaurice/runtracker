import * as express from "express";
import * as path from "path";
import { apiRequest } from "./api";
import logger from "./services/logger";

const PORT = ((process.env.port as unknown) as number) || 7800;

/**
 * Boots the express app with common middlewares.
 */
const app = express();

app.use(express.json());
app.use(express.urlencoded());

apiRequest(app);

// Serve any static files
app.use(express.static(path.join(__dirname, "./../../front/build")));

// Handle React routing, return all requests to React app
app.get("*", (_, res) => {
  res.sendFile(path.join(__dirname, "./../../front/build", "index.html"));
});

app.listen(PORT, "0.0.0.0", (err: Error) => {
  if (err) {
    throw err;
  }
  logger(`> Ready on http://0.0.0.0:${PORT}`);
});

export default app;
