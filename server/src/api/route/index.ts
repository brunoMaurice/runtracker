import { Application, Request, Response } from "express";
import { Route } from "../../db/models/route";
import logger from "./../../services/logger";

export const apiRoute = (srv: Application) => {
  srv.get(
    "/api/routes",
    async (req: Request, res: Response): Promise<Response> => {
      try {
        const routes = await req.container.getRepository(Route).find({
          where: {
            user: req.auth.user
          },
          order: {
            createdDate: "DESC"
          }
        });

        return res.send(routes);
      } catch (err) {
        logger(err);
        return res.sendStatus(500);
      }
    }
  );

  srv.post(
    "/api/connect",
    async (req: Request, res: Response): Promise<Response> => {
      try {
        const routeRepo = req.container.getRepository(Route);
        await routeRepo.save(
          routeRepo.create({
            createdBy: req.auth.user,
            name: req.body.name,
            geoJson: req.body.geoJson
          })
        );

        return res.send();
      } catch (err) {
        logger(err);
        return res.sendStatus(500);
      }
    }
  );
};
