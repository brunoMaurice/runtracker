import classNames from "classnames";
import * as React from "react";

export enum BUTTON_TAGS {
  DEFAULT,
  PRIMARY,
  SUCCESS,
  INFO,
  INFO2,
  WARNING,
  DANGER,
  LINK
}

interface IButtonProps {
  children: React.ReactNode;
  tags?: BUTTON_TAGS;
  onClick?: () => void;
  disabled?: boolean;
}

class Button extends React.Component<IButtonProps> {
  public render() {
    const tags = this.props.tags || BUTTON_TAGS.DEFAULT;
    return (
      <>
        <button
          type="button"
          disabled={this.props.disabled}
          onClick={this.props.onClick}
          className={classNames(
            "br2",
            "ba",
            "pa2",
            "ml1",
            "mv1",
            "border-box",
            {
              "bg-animate": !this.props.disabled,
              pointer: !this.props.disabled,
              "b--black": tags === BUTTON_TAGS.DEFAULT,
              "bg-white": tags === BUTTON_TAGS.DEFAULT,
              "hover-bg-light-gray": tags === BUTTON_TAGS.DEFAULT,
              "b--navy": tags === BUTTON_TAGS.PRIMARY,
              "bg-dark-blue": tags === BUTTON_TAGS.PRIMARY,
              "hover-bg-navy": tags === BUTTON_TAGS.PRIMARY,
              "b--dark-green": tags === BUTTON_TAGS.SUCCESS,
              "bg-green": tags === BUTTON_TAGS.SUCCESS,
              "hover-bg-dark-green": tags === BUTTON_TAGS.SUCCESS,
              "b--dark-blue": tags === BUTTON_TAGS.INFO,
              "bg-blue": tags === BUTTON_TAGS.INFO,
              "hover-bg-dark-blue": tags === BUTTON_TAGS.INFO,
              "b--blue": tags === BUTTON_TAGS.INFO2,
              "bg-light-blue": tags === BUTTON_TAGS.INFO2,
              "hover-bg-blue": tags === BUTTON_TAGS.INFO2,
              "b--orange": tags === BUTTON_TAGS.WARNING,
              "bg-gold": tags === BUTTON_TAGS.WARNING,
              "hover-bg-orange": tags === BUTTON_TAGS.WARNING,
              "b--dark-red": tags === BUTTON_TAGS.DANGER,
              "bg-red": tags === BUTTON_TAGS.DANGER,
              "hover-bg-dark-red": tags === BUTTON_TAGS.DANGER,
              "b--white": tags === BUTTON_TAGS.LINK,
              blue: tags === BUTTON_TAGS.LINK,
              "underline-hover": tags === BUTTON_TAGS.LINK,
              "hover-dark-blue": tags === BUTTON_TAGS.LINK,
              white: tags !== BUTTON_TAGS.LINK && tags !== BUTTON_TAGS.DEFAULT,
              "cursor-not-allowed": this.props.disabled,
              "o-60": this.props.disabled
            }
          )}
        >
          {this.props.children}
        </button>
      </>
    );
  }
}

export default Button;
