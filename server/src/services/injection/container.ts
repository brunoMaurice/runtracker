/* tslint:disable:max-classes-per-file */
import { Container as InversifyContainer } from "inversify";
import { Repository } from "typeorm";
import { servicesList } from "../../config/services";
import getRepositories from "../../db";
import { AbstractEntity } from "../../db/models/abstractEntity";
import { InstanceFactory } from "./instanceFactory";
import { ServiceFactory } from "./serviceFactory";

export type InstanciableService<T> = new () => T;

export class Container {
  private container: InversifyContainer;

  constructor() {}

  public async init() {
    this.container = new InversifyContainer();
    await this.initDbContainer();
    this.initServices();
  }

  /**
   * Get a service define in the config/services.ts
   */
  public getService = <T>(service: InstanciableService<T>): T => {
    return this.container.get<T>(service.name);
  };

  /**
   * Get repository using the entity name
   * The entity should be define into the IModel interface
   */
  public getRepository = <T>(
    repository: InstanciableService<T>
  ): Repository<T> => {
    return this.container.get(repository.name + "Repository");
  };

  /**
   * Allow mocking of any service
   * Only used in unit tests
   */
  public setServiceMock = <T>(serviceClass: InstanciableService<T>) => {
    return this.container
      .rebind<T>(serviceClass.name)
      .to(serviceClass)
      .inSingletonScope();
  };

  private initServices = (): void => {
    for (const service in servicesList) {
      if (!servicesList.hasOwnProperty(service)) {
        continue;
      }

      if (servicesList[service] === undefined) {
        throw new Error(`service : ${service} not correctly define`);
      }

      if (servicesList[service].kind === "factory") {
        this.bindClassWithFactory(servicesList[service]);
      } else if (servicesList[service].kind === "instanceFactory") {
        this.bindInstanceWithFactory(service, servicesList[service]);
      } else {
        this.bindClass(servicesList[service]);
      }
    }
  };

  private bindClass = <T>(service: new () => T): void => {
    this.container.bind<T>(service.name).to(service);
  };

  private bindClassWithFactory = (service: new () => ServiceFactory): void => {
    const instanciateService = new service();
    instanciateService.setDependency();

    this.container.bind(service.name).toConstantValue(instanciateService);
  };

  private bindInstanceWithFactory = (
    serviceName: string,
    service: InstanceFactory
  ): void => {
    service.setDependency();

    this.container.bind(serviceName).toConstantValue(service);
  };

  private initDbContainer = async (): Promise<void> => {
    const repositories = await getRepositories();

    for (const repoName in repositories) {
      if (!repositories.hasOwnProperty(repoName)) {
        continue;
      }

      this.container
        .bind<Repository<AbstractEntity>>(repoName + "Repository")
        .toConstantValue(repositories[repoName]);
    }
  };
}
