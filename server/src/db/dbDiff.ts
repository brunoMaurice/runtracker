/* tslint:disable:max-line-length */
/* tslint:disable:no-console */

import { exec } from "child_process";
import * as JsDiff from "diff";
import { DATABASE_CONNECTION_URI } from "../config";
import { initConnection } from "./";
import logger from "./../services/logger";

async function diff() {
  logger("Dumping schema from migration files");
  await asyncExec("yarn db:stop");
  await asyncExec("yarn db:start");
  await asyncExec("yarn db:migrate");
  await dropMigrationTable();
  const schemaMigrations = formatSQL(await getOrderedSQL());

  logger("Dumping schema from entity sync");
  await asyncExec("yarn db:stop");
  await asyncExec("yarn db:start");
  const connection = await initConnection();
  await connection.synchronize();
  const schemaSynced = formatSQL(await getOrderedSQL());

  logger("Creating schema diff");
  const schemaMigrationsSplitted = prepareDiff(schemaMigrations);
  const schemaSyncedSplitted = prepareDiff(schemaSynced);

  const arrayDiff = JsDiff.diffArrays(
    schemaMigrationsSplitted,
    schemaSyncedSplitted
  );

  const diffStr = arrayDiff
    .map(part => {
      if (part.added) {
        return `[+]\t${part.value}[=]`;
      }
      if (part.removed) {
        return `[-]\t${part.value}[=]`;
      }
      return `${part.value}`;
    })
    .join("")
    .replace(/\n(\s*)\[=\]/g, "[=]\n$1")
    .replace(/\n\s*\[(-|\+)\]/g, "\n[$1]");

  const diffIsDirty = arrayDiff.reduce(
    (agg, part) => agg || part.added || part.removed,
    false
  );

  if (diffIsDirty) {
    console.log(diffStr);
    process.exit(1);
  } else {
    console.log(
      "=============== SCHEMA FROM MIGRATIONS: \n" + schemaMigrations
    );
    console.log("=============== SCHEMA FROM ENTITIES: \n" + schemaSynced);
    console.log("===============\n");
    logger("Identical DB");
    process.exit(0);
  }
}

diff().then(
  () => process.exit(0),
  err => {
    console.log(err);
    logger(err);
    process.exit(1);
  }
);

function filterDeprecated(str: string): string {
  return str
    .split("\n")
    .filter(l => !l.includes("deprecated"))
    .join("\n");
}

function asyncExec(cmd: string): Promise<string> {
  return new Promise((resolve, reject) => {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        reject(err);
      }

      const sout = filterDeprecated(stdout);
      const serr = filterDeprecated(stderr);
      if (serr !== "") {
        return reject(new Error(serr));
      }
      serr.length > 0 ? reject(new Error(serr)) : resolve(sout);
    });
  });
}

async function getOrderedSQL() {
  return asyncExec(
    `docker run --net=host --name pg_dump --rm postgres sh -c "pg_dump ${DATABASE_CONNECTION_URI} --schema-only --no-acl --no-owner --quote-all-identifiers" | sed -e '/^--/d' | sed '/^\s*$/d'`
  );
}

async function dropMigrationTable() {
  return asyncExec(
    `docker run --net=host --name pg_dump --rm postgres sh -c "psql ${DATABASE_CONNECTION_URI} -c 'drop table migrations;'"`
  );
}

function prepareDiff(d: string) {
  d = d.replace(/CREATE FUNCTION (.|\n)* \$\$;/gm, "");

  return d
    .split("\n")
    .map(str => str.trim())
    .map(str => str.replace(/^,|,$/g, "")) // remove comas
    .map(str => str.replace(/(PK_[0-9a-f]*)/g, "SOME_KEY")) // rename constraints
    .map(str => str.replace(/([_a-z]*_pkey)/g, "SOME_KEY")) // rename constraints
    .map(str => str + "\n")
    .filter(str => !str.includes("COMMENT ON EXTENSION"))
    .filter(str => !str.includes("CREATE EXTENSION"))
    .filter(str => str.length > 0)
    .filter(str => str.match(/[a-zA-Z]/g))
    .sort();
}

function formatSQL(sql: string): string {
  return sql;
}
