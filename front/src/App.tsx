import * as React from "react";
import { Config } from "./config";
import Routes from "./Router";

class App extends React.Component {
  public render() {
    return (
      <div>
        <Config />
        <Routes />
      </div>
    );
  }
}

export default App;
