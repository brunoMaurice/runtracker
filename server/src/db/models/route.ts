import { Column, Entity, ManyToOne } from "typeorm";
import { AbstractEntity } from "./abstractEntity";
import { User } from "./user";

@Entity()
export class Route extends AbstractEntity {
  @ManyToOne(type => User, user => user.routes, { nullable: false })
  public createdBy: User;

  @Column({ nullable: false })
  public name: string;

  @Column({ nullable: false })
  public geoJson: string;
}
