declare module "react-draft-wysiwyg" {
  export interface EditorProps {
    initialContentState?: RawDraftContentState;
    defaultContentState?: RawDraftContentState;
    contentState?: RawDraftContentState;
    editorState?: EditorState;
    defaultEditorState?: EditorState;
    toolbarOnFocus?: boolean;
    spellCheck?: boolean;
    stripPastedStyles?: boolean;
    toolbar?: object;
    toolbarCustomButtons?: Array<React.ReactElement<HTMLElement>>;
    toolbarClassName?: string;
    toolbarHidden?: boolean;
    locale?: string;
    localization?: object;
    editorClassName?: string;
    wrapperClassName?: string;
    toolbarStyle?: object;
    editorStyle?: React.CSSProperties;
    wrapperStyle?: React.CSSProperties;
    mention?: object;
    hashtag?: object;
    textAlignment?: string;
    readOnly?: boolean;
    tabIndex?: number;
    placeholder?: string;
    ariaLabel?: string;
    ariaOwneeID?: string;
    ariaActiveDescendantID?: string;
    ariaAutoComplete?: string;
    ariaDescribedBy?: string;
    ariaExpanded?: string;
    ariaHasPopup?: string;
    wrapperId?: number;
    customDecorators?: object[];
    onChange?(contentState: ContentState): RawDraftContentState;
    onEditorStateChange?(editorState: EditorState): void;
    onContentStateChange?(contentState: ContentState): RawDraftContentState;
    uploadCallback?(file: object): Promise<object>;
    onFocus?(event: SyntheticEvent): void;
    onBlur?(event: SyntheticEvent): void;
    onTab?(event: SyntheticKeyboardEvent): void;
    onEscape?(e: SyntheticKeyboardEvent): void;
    onUpArrow?(e: SyntheticKeyboardEvent): void;
    onDownArrow?(e: SyntheticKeyboardEvent): void;
    onRightArrow?(e: SyntheticKeyboardEvent): void;
    onLeftArrow?(e: SyntheticKeyboardEvent): void;
    customBlockRenderFunc?(block: ContentBlock): any;
    editorRef?(ref: object): void;
    handlePastedText?(
      text: string,
      html: string,
      editorState: EditorState,
      onChange: (editorState: EditorState) => void
    ): boolean;
  }

  export class Editor extends React.Component<EditorProps> {
    constructor(props: Readonly<EditorProps>);
    public focusEditor(): void;
  }
}
