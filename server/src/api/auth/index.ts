import { Application } from "express";
import { Repository } from "typeorm";
import * as uuid from "uuid/v4";
import { CREATE_USER_ON_LOGIN } from "../../config";
import { User } from "../../db/models/user";
import { Connect } from "../../services/auth/connect";
import logger from "../../services/logger";

export const apiAuth = (srv: Application) => {
  srv.post(
    "/api/auth",
    async (req, res): Promise<void> => {
      try {
        const connect = req.container.getService(Connect);
        const jwt = await connect.connectUser(req.body.token);

        if (!jwt) {
          res.sendStatus(404);
          return;
        }

        res.send(jwt);
      } catch (err) {
        logger(err);
        res.sendStatus(500);
      }
    }
  );

  srv.post(
    "/api/connect",
    async (req, res): Promise<void> => {
      try {
        const userRepo: Repository<User> = req.container.getRepository(User);
        let user = await userRepo.findOne({ where: { email: req.body.email } });

        if (!user && CREATE_USER_ON_LOGIN) {
          user = await userRepo.save(
            userRepo.create({
              name: "unknown",
              email: req.body.email,
              trackingToken: uuid()
            })
          );
        }

        if (!user) {
          res.sendStatus(404);
          return;
        }

        const connect = req.container.getService(Connect);
        await connect.sendTokenLink(user);

        res.send();
      } catch (err) {
        logger(err);
        res.sendStatus(500);
      }
    }
  );
};
