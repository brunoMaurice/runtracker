import { Application } from "express";
import { apiAuth } from "./auth";
import { ContainerMiddleware } from "./middleware/container";
import { apiPosition } from "./position";

export const apiRequest = (srv: Application) => {
  ContainerMiddleware(srv);

  apiAuth(srv);
  apiPosition(srv);
};
