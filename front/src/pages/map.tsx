import * as React from "react";
import Page from "../components/layout/Page";
import MapContent from "./../components/map/MapContent";

class Map extends React.Component {
  public render() {
    return (
      <Page>
        <MapContent />
      </Page>
    );
  }
}

export default Map;
