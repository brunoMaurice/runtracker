import { Column, Entity, JoinColumn, OneToOne } from "typeorm";
import { AbstractEntity } from "./abstractEntity";
import { User } from "./user";

@Entity()
export class Auth extends AbstractEntity {
  @OneToOne(type => User, user => user.uuid, { nullable: false })
  @JoinColumn()
  public user: User;

  @Column({ nullable: false })
  public token: string;

  @Column({ nullable: false })
  public expirationDate: Date;
}
