export interface IServiceFactory {
  setDependency(): void;
}

export abstract class ServiceFactory implements IServiceFactory {
  public kind = "factory";
  public abstract setDependency = (): void => {};
}
